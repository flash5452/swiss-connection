import styled from 'styled-components/macro';
import {Link as ReachRouterLink} from 'react-router-dom';

export const Container = styled.div`
    height: 100px;
    display: flex;
    flex-wrap: wrap;
    margin: 0 56px;
    justify-content: center;
    
    @media (max-width: 1000px) {
    margin: 0 30px;
    }

    &:a {
        display: flex;
    }
`; 

export const Group = styled.div`
    display: flex;
    align-items: center;
`; 

export const Profile = styled.div`
    position:relative;    
    flex:1;
    margin-left:20px;
    text-align:justify;
    align-self: center;
`; 

export const Text = styled.p`
    color: black;
    font-size: 25px;
    line-height: normal;
    text-shadow: 2px 2px 4px rgba(252, 249, 249, 0.45);
`; 

export const LinkButton = styled(ReachRouterLink)`
    width: 100px;
    height: fit-content;
    display: block;    
    border-radius: 3px;
    border: 0;    
    padding: 8px 17px;
    font-size: 15px;
    background-color: rgb(131, 122, 122);
    color: #000;
    text-decoration: none;
    cursor: pointer;

    &:hover {
        background: #808080;
`; 

export const Link = styled(ReachRouterLink)`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const Button = styled.button`
    width: 100px;
    height: fit-content;
    display: block;    
    border-radius: 3px;
    border: 0;    
    padding: 8px 17px;
    font-size: 15px;
    background-color: rgb(131, 122, 122);
    color: #000;
    text-decoration: none;
    cursor: pointer;

    &:hover {
        background: #808080;
`; 

export const Frame = styled.div`
    height: 100px;
    display: flex;
    flex-wrap: wrap;
    margin: 0 56px;
    justify-content: center;
    
    @media (max-width: 1000px) {
    margin: 0 30px;
    }
`; 

export const BackButtonFrame = styled.p`

`;

export const HREF = styled.a`    
`;

export const Image = styled.img`
    height: 30px;       
    
    @media (min-width: 1449px) {
    height: 30px;      
    }
`;

export const TitleFrame = styled.div`
    display: flex;
    flex: 1;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const SignOutFrame = styled.div`
    flex: 0;
    align-self: center;   
`;