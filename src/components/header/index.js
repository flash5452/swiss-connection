import React from 'react';
import {
    Container,
    Group,
    Profile,
    Text,
    Button,
    LinkButton,
    Frame, 
    BackButtonFrame, 
    HREF,
    Image,
    TitleFrame,
    SignOutFrame, 
    Link
} from './styles/header';
export default function Header({children, ...restProps}){
    return <Container {...restProps}>{ children }</Container>
}

Header.Group = function HeaderGroup({children, ...restProps}){
    return <Group  {...restProps}>{ children }</Group>
}

Header.Profile = function HeaderProfile({children, ...restProps}){
    return <Profile {...restProps}>{children}</Profile>
}

Header.Text = function HeaderText({children, ...restProps}){
    return <Text {...restProps}>{children}</Text>
}

Header.Button = function HeaderButton({children, ...restProps}){
    return  <Button {...restProps}>{children}</Button>
}

Header.LinkButton = function HeaderLinkButton({children, ...restProps}){
    return <LinkButton {...restProps}>{children}</LinkButton>
}

Header.Link = function HeaderLink({children, ...restProps}){
    return <Link {...restProps}>{children}</Link>
}

Header.Frame = function HeaderFrame({children, ...restProps}){
    return <Frame {...restProps}>{children}</Frame>
}

Header.BackButtonFrame = function HeaderBackButtonFrame({children, ...restProps}){
    return <BackButtonFrame {...restProps}>{children}</BackButtonFrame>
}

Header.HREF = function HeaderHREF({children, ...restProps}){
    return <HREF {...restProps}>{children}</HREF>
}

Header.Image = function HeaderImage({children, ...restProps}){
    return <Image { ...restProps}>{children}</Image>
}

Header.TitleFrame = function HeaderTitleFrame({children, ...restProps}){
    return <TitleFrame { ...restProps}>{children}</TitleFrame>
}

Header.SignOutFrame = function HeaderSignOutFrame({children, ...restProps}){
    return <SignOutFrame { ...restProps}>{children}</SignOutFrame>
}