import React from 'react';
import {
    Section,
    SectionTitle, 
    SectionError,
    SectionTitleNoData
} from './styles/pageSection';
export default function PageSection({children, ...restProps}){
    return <Section {...restProps}>{children}</Section>
}

PageSection.Title = function PageSectionPageTitle({children, ...restProps}){
    return  <SectionTitle {...restProps}>{children}</SectionTitle>
}

PageSection.SectionError = function PageSectionPageSectionError({children, ...restProps}){
    return  <SectionError {...restProps}>{children}</SectionError>
}

PageSection.TitleNoData = function PageSectionPageTitleNoData({children, ...restProps}){
    return  <SectionTitleNoData {...restProps}>{children}</SectionTitleNoData>
}