import styled from 'styled-components/macro';

export const Section = styled.div`
    display: flex;
    flex-direction: column;  
    margin: 10px 56px;
    align-items:center;     
    justify-content: center;
`;

export const SectionTitle = styled.span`
    margin-top: 10px;
    font-size: 20px;
    text-decoration: underline;
`;

export const SectionError = styled.span`
    margin: 0 0 16px;
    border-radius: 4px;
    padding: 15px 20px;
    font-size: 14px;
    color: black;
    background: #e87c03;
`;

export const SectionTitleNoData = styled.span`
    margin-top: 10px;
    font-size: 20px;
`;
