import React, {useState}  from 'react';
import '../../lib/fontawesome';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { setFavouriteStation, deleteFavouriteStation } from '../../services/firebase';
import{
    Container,
    Group,
    Title,
    Text,
    SubText,
    Link,
    Item,
    FavIcon
} from './styles/card'

export default function Card({children, stationDetail, stationName, ...restProps}){
    stationDetail = stationDetail || {favourite: null};    
    const [fav, setFav] = useState(stationDetail.favourite); 
    const [error, setError] = useState(''); 
    const handleClick = (event) => {
        event.preventDefault();
        setFav(prev => !prev );   
        stationDetail.favourite = !stationDetail.favourite;    
        !fav ? setFavouriteStation(stationName, stationDetail).catch((error) => { setError(error.message)}) 
        : deleteFavouriteStation(stationDetail.docId).catch((error)=>{ setError(error.message) });
    }

    return (
        <Container {...restProps}>                        
            { fav != null ? <FavIcon onClick={handleClick}>{fav ? <FontAwesomeIcon icon={['fas', 'heart']}/> : <FontAwesomeIcon icon={['far', 'heart']}/> }</FavIcon> : null }
            {children}
        </Container>
    )   
}

Card.Title = function CardTitle({children, ...restProps}){
    return <Title {...restProps}>{children}</Title>
}

Card.Group = function CardGroup({children, ...restProps}) {
    return <Group {...restProps}>{children}</Group>
}

Card.Text = function CardText({children, ...restProps}) {
    return <Text {...restProps}>{children}</Text>
}

Card.SubText = function CardSubText({children, ...restProps}) {
    return <SubText {...restProps}>{children}</SubText>
}

Card.Link = function CardLink({children, ...restProps}) {
    return <Link {...restProps}>{children}</Link>
}

Card.Item = function CardItem({children, ...restProps}) {
    return <Item {...restProps}>{children}</Item>
}