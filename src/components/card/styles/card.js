import styled from 'styled-components/macro';
import { Link as ReachRouterLink } from 'react-router-dom';

export const Container = styled.div`
    width: 300px;
    height: fit-content;
    display: flex;
    flex-direction: column;
    margin: 25px;
    padding: 10px 15px;
    border-radius: 10px;
    border-style: solid;
    align-items: flex-start;

    &:last-of-type {
        margin-bottom: 0;
    }
`; 

export const Title = styled.h1`
    display: flex;
    flex-direction: column;
    margin: 10px 56px;
    align-items: center;
    justify-content: center;   
`;

export const Group= styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 56px;
    align-items: center;
    justify-content: center;
`;

export const Text = styled.p`
    font-size: 18px;
    align-self: center;
    color: #000;    
    user-select: none;
    line-height: normal;
`; 

export const SubText = styled.p`
    margin-left: 5px;
    align-self: center;
    font-size: 15px;
    color: #000;    
    user-select: none;
    line-height: normal;
`; 

export const Link = styled(ReachRouterLink)`
    margin-right: 30px;
    color: #000;
    text-decoration: none;
    cursor: pointer;

    &:hover {
        font-weight: bold;
    }
`; 

export const Item = styled.div`
    display:flex;
    flex-direction: row;
    height: 50px;
    align-items: center;
`; 

export const FavIcon = styled.div`
    display: flex;
    flex-direction: row-reverse;
    width: -webkit-fill-available;
`;