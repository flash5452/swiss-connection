import styled from 'styled-components/macro';
import { Link as ReachRouterLink } from 'react-router-dom'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin:0 56px;    
    padding:60px 68px;
    border-radius: 5px;
`;

export const Title = styled.h1`
    margin-bottom: 28px;
    font-weight: bold;
    font-size: 28px;
    color: #000;
`;

export const Error = styled.span`
    margin: 0 0 16px;
    border-radius: 4px;
    padding: 15px 20px;
    font-size: 14px;
    color: black;
    background: #e87c03;
`;

export const Base = styled.form`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 350px;
`;

export const Input = styled.input`
    height: 50px;
    margin-bottom: 20px;
    border-radius: 4px;
    border: 0;
    padding: 5px 20px;
    background: rgb(226, 224, 224);
    line-height: 50px;
    color: #000;

    &:last-of-type {
        margin-bottom: 30px;
    }
`;

export const Button = styled.button`
    margin: 24px 0 12px;
    border:0;
    border-radius: 4px;
    padding: 16px;
    font-weight: bold;
    font-size: 16px;
    color: #000;
    background: rgb(131, 122, 122);
    cursor: pointer;

    &:disabled {
        opacity: 0.5;
    }
`;

export const Text = styled.p`
    margin-top: 10px;
    font-size: 13px;    
    color: 3737373;
    line-height: normal;
`;

export const Link = styled(ReachRouterLink)`
    color:#000;
    text-decoration: none;
    &:hover {
        text-decoration: underline;
    }    
`;