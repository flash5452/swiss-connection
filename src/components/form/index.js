import React from 'react';
import {
    Container, 
    Title,
    Error,
    Base,
    Input, 
    Button,
    Text,
    Link
} from './styles/form'
export default function Form({children, ...restProps}){
    return <Container {...restProps}>{children}</Container>
}

Form.Title = function FormTitle({children, ...restProps}){
    return <Title {...restProps}>{children}</Title>
}

Form.Error = function FormError({children, ...restProps}) {
    return <Error {...restProps}>{children}</Error>
}

Form.Base = function FormBase({children, ...restProps}) {
    return <Base {...restProps}>{children}</Base>
}

Form.Input = function FormInput({children, ...restProps}) {
    return <Input {...restProps}>{children}</Input>
}

Form.Button = function FormButton({children, ...restProps}) {
    return <Button {...restProps}>{children}</Button>
}

Form.Text = function FormText({children, ...restProps}) {
    return <Text {...restProps}>{children}</Text>
}

Form.Link = function FormLink({children, ...restProps}) {
    return <Link {...restProps}>{children}</Link>
}