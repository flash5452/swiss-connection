import { firebase } from '../lib/firebase.prod';

export async function getFavouriteStationList(){
  const favStationRef = firebase.firestore().collection("favouriteStationDetails");
  var favStationList = {};
  return await favStationRef
    .get()
    .then(snapshot => {      
      favStationList = snapshot.docs.map( doc => ({       
        ...doc.data()
      }));
      return favStationList;
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });         
}

export async function deleteFavouriteStation(docId){
  await firebase.firestore().collection("favouriteStationDetails").doc(docId).delete().then(function() {
      console.log("Document successfully deleted!");
  }).catch(function(error) {
      console.error("Error removing document: ", error);
  });
}

export async function getStationDetails(stationName){     
  const favStationList = await getFavouriteStationList(); 
  const data = await fetch(`http://transport.opendata.ch/v1/stationboard?station=${stationName}&limit=10`)
    .then(response => response.json())
    .then(result => {
      return result;
    })
    .catch(error => console.log('error', error));
  
  const response = {};    
  response['station'] = data.station;
  //setting unique docId
  response['stationboard'] = data.stationboard.map((item)=>({   
    ...item, 
    docId: item.name + item.stop.departureTimestamp   
  }));  
  //setting if station is favoutie
  response.stationboard.forEach(function (stationDetail) {    
    if (favStationList.filter(favStation => favStation.docId == stationDetail.docId).length > 0){  
      stationDetail.favourite = true
    } 
    else
    { 
      stationDetail.favourite = false; 
    }
  }); 
  
  return response;                       
}

export async function setFavouriteStation(stationName, stationDetails){
  return firebase
    .firestore()
    .collection("favouriteStationDetails")
    .doc(stationDetails.docId)
    .set({
      stationName: stationName,
      docId:stationDetails.docId,
      stationDetails: stationDetails
    })
    .then(function() {
      console.log("Doc successful");
  })
  .catch(function(error) {
     console.error("Error writing doc", error);
  });
}