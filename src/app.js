import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { useAuthListener } from './services';
import { IsUserRedirect, ProtectedRoute } from './helpers/routes';
import * as ROUTES from './constants/routes';
import { Home, SignIn, SignUp, Station, RouteSearch, Favourite, FavouriteStation } from './pages';

export function App() {
  const { user } = useAuthListener();
  return (
    <Router>
      <Switch>              
        <IsUserRedirect user={user} loggedInPath={ROUTES.HOME} path={ROUTES.SIGN_IN}>
          <SignIn />
        </IsUserRedirect>
        <IsUserRedirect user={user} loggedInPath={ROUTES.HOME} path={ROUTES.SIGN_UP}>
          <SignUp />
        </IsUserRedirect>        
        <ProtectedRoute user={user} path={ROUTES.HOME}>
          <Home />
        </ProtectedRoute>
        <ProtectedRoute user={user} path={ROUTES.STATION}>                        
          <Station />
        </ProtectedRoute>                            
        <ProtectedRoute user={user}ute path={ROUTES.FAVOURITESTATION}>
          <FavouriteStation />
        </ProtectedRoute>
        <IsUserRedirect user={user} loggedInPath={ROUTES.HOME} path={ROUTES.NOADDR}>
          <Home />
        </IsUserRedirect>
      </Switch>
    </Router>
  );
}
