import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// 1) when seeding the database you'll have to uncomment this!
//import { seedDatabase } from '../seed';

const config = {
  apiKey: "AIzaSyBEw64nngA464r4E_I0Z6wz2pa52U_HO38",
  authDomain: "swiss-connection.firebaseapp.com",
  databaseURL: "https://swiss-connection-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "swiss-connection",
  storageBucket: "swiss-connection.appspot.com",
  messagingSenderId: "1000201672495",
  appId: "1:1000201672495:web:3aaf0a17925f4ea1de33da",
  measurementId: "G-MN49BCDLW1"
};

const firebase = Firebase.initializeApp(config);
// 2) when seeding the database you'll have to uncomment this!
//seedDatabase(firebase);
// 3) once you have populated the database (only run once!), re-comment this so you don't get duplicate data

export { firebase };
