import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import { faArrowAltCircleLeft } from '@fortawesome/free-regular-svg-icons';

library.add(faHeartSolid, faHeartRegular, faArrowAltCircleLeft);