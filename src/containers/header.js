import React from 'react';
import * as ROUTES from '../constants/routes';
import { Header } from '../components'

{/*
export function <functionName>({children}){
    return(
        <div className='headerContainer'>
            {children}
        </div>
    );
}
*/}

export function HeaderContainer({children}){
    return(
        <Header>
            <Header.Frame>                
                <Header.LinkButton to={ROUTES.SIGN_IN}>Sign In</Header.LinkButton>                
            </Header.Frame>
            {children}
        </Header>
    );
}