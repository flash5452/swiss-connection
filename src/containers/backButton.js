import React from 'react';
import Header from "../components/header";
import back from '../assets/back-button.svg';
import '../lib/fontawesome';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function BackButtonContainer({children, to, title}){
    return(
        <Header>            
                <Header.Link to={to}>
                    <Header.BackButtonFrame>    
                        <FontAwesomeIcon icon={['far','arrow-alt-circle-left']} color="#000000" size="2x" />    
                        {/*<Header.Image src={back}/>*/}
                    </Header.BackButtonFrame>
                </Header.Link>
                <Header.TitleFrame>
                    <Header.Text>{title}</Header.Text>
                </Header.TitleFrame>             
        </Header> 
    );
}