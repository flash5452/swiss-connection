import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { FirebaseContext } from '../context/firebase';
import * as ROUTES from '../constants/routes';
import { HeaderContainer } from '../containers/header';
import { Form } from '../components';

export default function SignIn() {
    const history = useHistory();
    const { firebase } = useContext(FirebaseContext);

    const [emailAddress, setEmailAddress] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const isInvalid = password === '' || emailAddress === '';

    const handleSignin = (event) => {
        event.preventDefault();

        return firebase
            .auth()
            .signInWithEmailAndPassword(emailAddress, password)
            .then(() => {                                                        
                history.push(ROUTES.HOME);                    
            })
            .catch((error) => {              
                setEmailAddress('');
                setPassword('');
                setError(error.message);
            });   
    };
    return (
        <>
            <HeaderContainer>
                <Form>
                    <Form.Title>Sign In</Form.Title>
                    {error && <Form.Error>{error}</Form.Error>}
                    <Form.Base onSubmit={handleSignin} method="POST">
                        <Form.Input                                                       
                            placeholder="Email Address"
                            value={emailAddress}
                            onChange={({target}) => setEmailAddress(target.value)}
                        />
                        <Form.Input  
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={({target}) => setPassword(target.value)}
                            autoComplete="off"
                        />
                        <Form.Button disabled={isInvalid} type="submit" data-testid="sign-in"> 
                            Login
                        </Form.Button>   
                        <Form.Text>
                            New to App? <Form.Link to='/signup'> Sign up now </Form.Link>
                        </Form.Text>                     
                    </Form.Base>                    
                </Form>                
            </HeaderContainer>           
        </>
    );
}
