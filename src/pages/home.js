import React, {useContext} from 'react';
import { FirebaseContext } from '../context/firebase';
import { Header, Card } from '../components';
import * as ROUTES from '../constants/routes';

export default function Home(){
    const { firebase } = useContext(FirebaseContext);    
    const user = firebase.auth().currentUser || {};    

    return(
        <>            
            <Header>                
                <Header.Profile>
                    <Header.Text>Hello {user.displayName}</Header.Text>
                </Header.Profile>            
                <Header.SignOutFrame>    
                    <Header.Button onClick={() => firebase.auth().signOut()}> Sign Out </Header.Button> 
                </Header.SignOutFrame>
            </Header>                               
            <Card.Group>                
                <Card.Link to={ROUTES.STATION}>                    
                    <Card>
                        <Card.Text>Station</Card.Text>
                    </Card>
                </Card.Link>                
                <Card.Link to={ROUTES.FAVOURITESTATION}>
                    <Card>
                        <Card.Text>Favourite Station</Card.Text>
                    </Card>
                </Card.Link>                                            
            </Card.Group>
        </>
    );
}
