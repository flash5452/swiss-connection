import React, {useState, useEffect} from 'react';
import { getStationDetails } from '../services/firebase';
import {Card, PageSection} from '../components';
import Moment from 'react-moment';

export default function StationDetail({stationName, formSubmitted}){    
    const [stationDetails, setStationDetails] = useState({}); 
    const [error, setError] = useState('');  

    useEffect(() => {
        (async () => {                        
            stationName && setStationDetails(
                await getStationDetails(stationName).catch((error) => {                              
                setError(error.message);
            }));                                  
        })();
    },[formSubmitted]);  

    return Object.entries(stationDetails).length !== 0 ? (                    
        <>                            
            <PageSection>                
                <PageSection.Title>Station Details</PageSection.Title>  
                {error && <PageSection.Error>{error}</PageSection.Error>}                                             
            </PageSection>                    
            <Card.Group>                                                
                <Card.Title> From: {stationDetails.station.name} </Card.Title>                
                {   stationDetails.stationboard.map((item) => (                      
                    <Card key={`${item.docId}`} stationDetail={item} stationName={stationName}>                                                                                                                                                               
                        <Card.Item><Card.Text>To:</Card.Text><Card.SubText>{item.to}</Card.SubText></Card.Item>                
                        <Card.Item><Card.Text>Transport category:</Card.Text><Card.SubText>{item.category}</Card.SubText></Card.Item>
                        <Card.Item><Card.Text>Transport Number:</Card.Text><Card.SubText>{item.number}</Card.SubText></Card.Item>  
                        <Card.Item><Card.Text>Transport Operator:</Card.Text><Card.SubText>{item.operator ? item.operator : 'NA'}</Card.SubText></Card.Item>   
                        <Card.Item><Card.Text>Transport Name:</Card.Text><Card.SubText>{item.name}</Card.SubText></Card.Item>                      
                        <Card.Item><Card.Text>Departure Date:</Card.Text><Card.SubText><Moment format="YYYY/MM/DD">{item.stop.departure}</Moment></Card.SubText></Card.Item>                                                                        
                        <Card.Item><Card.Text>Departure Time:</Card.Text><Card.SubText><Moment format="HH:mm">{item.stop.departure}</Moment></Card.SubText></Card.Item>                                                                        
                    </Card>
                ))}                                  
            </Card.Group>                        
        </>
    ) : (
        <>            
            <PageSection>                               
                <PageSection.TitleNoData>No Details Present !!</PageSection.TitleNoData>
            </PageSection>
        </>    
    );
}
