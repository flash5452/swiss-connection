import React, {useState, useContext} from 'react';
import { useHistory } from 'react-router-dom';
import { FirebaseContext } from '../context/firebase';
import * as ROUTES from '../constants/routes';
import { HeaderContainer } from '../containers/header';
import { Form } from '../components';

export default function SignUp() {
    const history = useHistory();
    const { firebase } = useContext(FirebaseContext);

    const [firstName, setFirstName] = useState('');
    const [emailAddress, setEmailAddress] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const isInvalid = firstName === '' || password === '' || emailAddress === '';

    const handleSignup = (event) => {
        event.preventDefault();

        return firebase
            .auth()
            .createUserWithEmailAndPassword(emailAddress, password)
            .then((result) => {
                result.user
                    .updateProfile({
                        displayName: firstName                        
                    })
                    .then(() => {
                        history.push(ROUTES.HOME);
                    })
                })
            .catch((error) => {
                setFirstName('');
                setEmailAddress('');
                setPassword('');
                setError(error.message);
            });   
    };

    return (
        <>
            <HeaderContainer>
                <Form>
                    <Form.Title>Sign Up</Form.Title>
                    {error && <Form.Error data-testid="error">{error}</Form.Error>}
                    <Form.Base onSubmit={handleSignup} method="POST">
                        {/*
                        <input                         
                            placeholder=""
                            value={}
                            onChange={({target}) => }
                        />
                        */}
                        <Form.Input                       
                            placeholder="First name"
                            value={firstName}
                            onChange={({target}) => setFirstName(target.value)}
                        />
                        <Form.Input                         
                            placeholder="Email Address"
                            value={emailAddress}
                            onChange={({target}) => setEmailAddress(target.value)}
                        />
                        <Form.Input
                            type = "password" 
                            placeholder = "Password"
                            value={password}
                            onChange={({target}) => setPassword(target.value)}
                            autoComplete="off"
                        />
                        <Form.Button disabled={isInvalid} type="submit" data-testid="sign-up"> 
                            Sign Up
                        </Form.Button>   
                        <Form.Text>
                            Already a user ? <Form.Link to="/signin">Sign in now</Form.Link>
                        </Form.Text>                     
                    </Form.Base>                    
                </Form>                
            </HeaderContainer> 
        </>
    )
}
