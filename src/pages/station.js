import React, {useState} from 'react';
import { Form } from '../components';
import { StationDetail } from '../pages';
import BackButtonContainer from '../containers/backButton';
import * as ROUTES from '../constants/routes';

export default function Station(){   
    const [stationName, setStationName] = useState('');
    const [formSubmitted, setFormSubmitted] = useState(false);
    const handleSubmit =  (event) => {
        event.preventDefault();        
        setFormSubmitted(prev => !prev);
    }
    return (
        <>            
            <BackButtonContainer title='Station' to={ROUTES.HOME}/>                                                               
            <Form>
                <Form.Base onSubmit={handleSubmit}>                
                    {/*<select name = 'location-type' />*/}
                    <Form.Input 
                        type="text" 
                        placeholder="Name of Station"
                        value={stationName} 
                        onChange={({target}) => setStationName(target.value)}                   
                    />
                    <Form.Button type='submit'> Submit </Form.Button>
                </Form.Base>      
            </Form>            
            <StationDetail stationName = {stationName} formSubmitted={formSubmitted} />            
        </>
    )
}
