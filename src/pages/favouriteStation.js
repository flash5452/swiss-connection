import React, {useState, useEffect} from 'react';
import { getFavouriteStationList } from '../services/firebase';
import {Card, PageSection} from '../components';
import Moment from 'react-moment';
import Countdown from 'react-countdown';
import BackButtonContainer from '../containers/backButton';
import * as ROUTES from '../constants/routes';

export default function FavouriteStation(){    //TD add tab selectec
    const [favStation, setFavStation] = useState({}); 
    const [error, setError] = useState('');  
    const renderer = ({ hours, minutes, seconds, completed}) => {
        if(completed) {
            return <span style={{marginLeft: '5px'}}>Time Over!</span>;
        } else {
            return <span style={{marginLeft: '5px'}}>{hours}:{minutes}:{seconds}</span>;
        }
    };

    useEffect(() => {
        (async () => {                        
            setFavStation(
                await getFavouriteStationList().catch((error) => {                              
                setError(error.message);
            }));                                  
        })();
    },[]);  //TD add tab selectec

    return Object.entries(favStation).length !== 0 ? (                            
        <>                                        
            <BackButtonContainer title='Favourite Station' to={ROUTES.HOME}/>
            <PageSection>                
                {/*<PageSection.Title>Favourite Station</PageSection.Title>*/}  
                {error && <PageSection.Error>{error}</PageSection.Error>}                                             
            </PageSection>                    
            <Card.Group>                
                {favStation.map((item) => (                                                                                                                                                                                              
                    <Card key={`${item.docId}`} stationName={item.stationName} stationDetail={item.stationDetails}>                                                                                                                       
                        <Card.Item><Card.Text>Time Left:</Card.Text><Countdown renderer={renderer} date={item.stationDetails.stop ? item.stationDetails.stop.departure : 'NA' }><Card.SubText>Time Over !</Card.SubText></Countdown></Card.Item>
                        <Card.Item><Card.Text>From:</Card.Text><Card.SubText>{item.stationName}</Card.SubText></Card.Item>
                        <Card.Item><Card.Text>To:</Card.Text><Card.SubText>{item.stationDetails.to}</Card.SubText></Card.Item>                
                        <Card.Item><Card.Text>Transport category:</Card.Text><Card.SubText>{item.stationDetails.category}</Card.SubText></Card.Item>
                        <Card.Item><Card.Text>Transport Number:</Card.Text><Card.SubText>{item.stationDetails.number}</Card.SubText></Card.Item>  
                        <Card.Item><Card.Text>Transport Operator:</Card.Text><Card.SubText>{item.stationDetails.operator ? item.stationDetails.operator : 'NA'}</Card.SubText></Card.Item>   
                        <Card.Item><Card.Text>Transport Name:</Card.Text><Card.SubText>{item.stationDetails.name}</Card.SubText></Card.Item>                                    
                        <Card.Item><Card.Text>Departure Date:</Card.Text><Card.SubText><Moment format="YYYY/MM/DD">{item.stationDetails.stop ? item.stationDetails.stop.departure : 'NA' }</Moment></Card.SubText></Card.Item>                                                                        
                        <Card.Item><Card.Text>Departure Time:</Card.Text><Card.SubText><Moment format="HH:mm">{item.stationDetails.stop ? item.stationDetails.stop.departure : 'NA'}</Moment></Card.SubText></Card.Item>                                                                        
                    </Card>                                    
                ))}                                  
            </Card.Group>                        
        </>
    ) : (
        <>            
            <PageSection>                               
                <PageSection.TitleNoData>No Details Present !!</PageSection.TitleNoData>
            </PageSection>
        </>    
    );
}
