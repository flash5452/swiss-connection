export function seedDatabase(firebase) {
  /* locations
    ============================================ */
  firebase.firestore().collection('locations').add({
    "id": "1",
    "name": "Basel",
    "score": null,
    "coordinate": {
        "type": "WGS84",
        "x": 47.547408,
        "y": 7.589547
    },
    "distance": null
  });
  firebase.firestore().collection('locations').add({
    "id": "2",
    "name": "Laussane",
    "score": null,
    "coordinate": {
        "type": "WGS84",
        "x": 47.547408,
        "y": 7.589547
    },
    "distance": null
  });
  firebase.firestore().collection('locations').add({
    "id": "3",
    "name": "Zurich",
    "score": null,
    "coordinate": {
        "type": "WGS84",
        "x": 47.547408,
        "y": 7.589547
    },
    "distance": null
  });
  firebase.firestore().collection('locations').add({
    "id": "4",
    "name": "Basel",
    "score": null,
    "coordinate": {
        "type": "WGS84",
        "x": 47.547408,
        "y": 7.589547
    },
    "distance": null
  });
  firebase.firestore().collection('locations').add({
    "id": "5",
    "name": "Bern",
    "score": null,
    "coordinate": {
        "type": "WGS84",
        "x": 47.547408,
        "y": 7.589547
    },
    "distance": null
  });


  /* connections
    ============================================ */
  firebase.firestore().collection('connections').add({
    "from" : {
        "arrival" : null,
        "arrivalTimestamp" : null,
        "departure" : "2012-03-31T08:58:00+02:00",
        "departureTimestamp" : 1333177080,
        "platform" : "7",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : "-1",
            "capacity2nd" : "-1"
        },
        "station" : {
            "id": "1",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    },
    "to" : {
        "arrival" : "2012-03-31T09:46:00+02:00",
        "arrivalTimestamp" : 1333179960,
        "departure" : null,
        "departureTimestamp" : null,
        "platform" : "2",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : null,
            "capacity2nd" : null
        },
        "station" : {
            "id": "2",
            "name": "Laussane",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    }
  });
  firebase.firestore().collection('connections').add({
    "from" : {
        "arrival" : null,
        "arrivalTimestamp" : null,
        "departure" : "2012-03-31T08:58:00+02:00",
        "departureTimestamp" : 1333177080,
        "platform" : "7",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : "-1",
            "capacity2nd" : "-1"
        },
        "station" : {
            "id": "3",
            "name": "Zurich",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    },
    "to" : {
        "arrival" : "2012-03-31T09:46:00+02:00",
        "arrivalTimestamp" : 1333179960,
        "departure" : null,
        "departureTimestamp" : null,
        "platform" : "2",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : null,
            "capacity2nd" : null
        },
        "station" : {
            "id": "4",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    }
  });
  firebase.firestore().collection('connections').add({
    "from" : {
        "arrival" : null,
        "arrivalTimestamp" : null,
        "departure" : "2012-03-31T08:58:00+02:00",
        "departureTimestamp" : 1333177080,
        "platform" : "7",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : "-1",
            "capacity2nd" : "-1"
        },
        "station" : {
            "id": "5",
            "name": "Bern",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    },
    "to" : {
        "arrival" : "2012-03-31T09:46:00+02:00",
        "arrivalTimestamp" : 1333179960,
        "departure" : null,
        "departureTimestamp" : null,
        "platform" : "2",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : null,
            "capacity2nd" : null
        },
        "station" : {
            "id": "1",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            },
            "distance": null
        }
    }
});
  firebase.firestore().collection('connections').add({
    "from" : {
        "arrival" : null,
        "arrivalTimestamp" : null,
        "departure" : "2012-03-31T08:58:00+02:00",
        "departureTimestamp" : 1333177080,
        "platform" : "7",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : "-1",
            "capacity2nd" : "-1"
        },
        "station" : {
            "id": "2",
            "name": "Laussane",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    },
    "to" : {
        "arrival" : "2012-03-31T09:46:00+02:00",
        "arrivalTimestamp" : 1333179960,
        "departure" : null,
        "departureTimestamp" : null,
        "platform" : "2",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : null,
            "capacity2nd" : null
        },
        "station" : {
            "id": "3",
            "name": "Zurich",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    }
  });
  firebase.firestore().collection('connections').add({
    "from" : {
        "arrival" : null,
        "arrivalTimestamp" : null,
        "departure" : "2012-03-31T08:58:00+02:00",
        "departureTimestamp" : 1333177080,
        "platform" : "7",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : "-1",
            "capacity2nd" : "-1"
        },
        "station" : {
            "id": "4",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    },
    "to" : {
        "arrival" : "2012-03-31T09:46:00+02:00",
        "arrivalTimestamp" : 1333179960,
        "departure" : null,
        "departureTimestamp" : null,
        "platform" : "2",
        "prognosis" : {
            "platform" : null,
            "arrival" : null,
            "departure" : null,
            "capacity1st" : null,
            "capacity2nd" : null
        },
        "station" : {
            "id": "5",
            "name": "Bern",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        }
    }
  });

  /* stationBoard
    ============================================ */
  firebase.firestore().collection('stationBoard').add({
    "stop":
    {
        "station":
        {
            "id": "1",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        },
        "arrival": null,
        "arrivalTimestamp": null,
        "departure": "2012-03-31T14:39:00+02:00",
        "departureTimestamp": 1333197540,
        "platform": "",
        "prognosis":
        {
            "platform": null,
            "arrival": null,
            "departure": null,
            "capacity1st": "-1",
            "capacity2nd": "-1"
        }
    },
    "name": "BUS13543",
    "category": "BUS",
    "number": "13543",
    "operator": null,
    "to": "Frick"
  });
  firebase.firestore().collection('stationBoard').add({
    "stop":
    {
        "station":
        {
            "id": "2",
            "name": "Laussane",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        },
        "arrival": null,
        "arrivalTimestamp": null,
        "departure": "2012-03-31T14:39:00+02:00",
        "departureTimestamp": 1333197540,
        "platform": "",
        "prognosis":
        {
            "platform": null,
            "arrival": null,
            "departure": null,
            "capacity1st": "-1",
            "capacity2nd": "-1"
        }
    },
    "name": "Bus 2",
    "category": "Bus",
    "number": "2",
    "operator": "BBA",
    "to": "Zurich"
  });
  firebase.firestore().collection('stationBoard').add({
    "stop":
    {
        "station":
        {
            "id": "3",
            "name": "Zurich",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        },
        "arrival": null,
        "arrivalTimestamp": null,
        "departure": "2012-03-31T14:39:00+02:00",
        "departureTimestamp": 1333197540,
        "platform": "",
        "prognosis":
        {
            "platform": null,
            "arrival": null,
            "departure": null,
            "capacity1st": "-1",
            "capacity2nd": "-1"
        }
    },
    "name": "Bus 2",
    "category": "Bus",
    "number": "2",
    "operator": "BBA",
    "to": "Montreux"
  });
  firebase.firestore().collection('stationBoard').add({
    "stop":
    {
        "station":
        {
            "id": "4",
            "name": "Basel",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        },
        "arrival": null,
        "arrivalTimestamp": null,
        "departure": "2012-03-31T14:39:00+02:00",
        "departureTimestamp": 1333197540,
        "platform": "",
        "prognosis":
        {
            "platform": null,
            "arrival": null,
            "departure": null,
            "capacity1st": "-1",
            "capacity2nd": "-1"
        }
    },
    "name": "Bus 2",
    "category": "Bus",
    "number": "2",
    "operator": "BBA",
    "to": "Geneva"
  });
  firebase.firestore().collection('stationBoard').add({
    "stop":
    {
        "station":
        {
            "id": "5",
            "name": "Bern",
            "score": null,
            "coordinate": {
                "type": "WGS84",
                "x": 47.547408,
                "y": 7.589547
            }
        },
        "arrival": null,
        "arrivalTimestamp": null,
        "departure": "2012-03-31T14:39:00+02:00",
        "departureTimestamp": 1333197540,
        "platform": "",
        "prognosis":
        {
            "platform": null,
            "arrival": null,
            "departure": null,
            "capacity1st": "-1",
            "capacity2nd": "-1"
        }
    },
    "name": "Bus 2",
    "category": "Bus",
    "number": "2",
    "operator": "BBA",
    "to": "Zurich"
  }); 
}
