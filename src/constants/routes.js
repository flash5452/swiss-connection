export const NOADDR = '/';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const HOME = '/home';
export const STATION = '/station';
export const STATIONDETAIL = '/stationDetail';
export const FAVOURITE= '/favourite';
export const FAVOURITESTATION = '/favouriteStation';